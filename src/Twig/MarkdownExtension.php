<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 17:03
 */

namespace TwoDevs\Bundle\MarkdownBundle\Twig;

use TwoDevs\Bundle\MarkdownBundle\Parser\ParserManager;

class MarkdownExtension extends \Twig_Extension
{
    /** @var ParserManager */
    private $parserManager;

    /**
     * MarkdownExtension constructor.
     * @param ParserManager $parserManager
     */
    public function __construct(ParserManager $parserManager)
    {
        $this->parserManager = $parserManager;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('markdown', [$this, 'markdown'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('md', [$this, 'markdown'], ['is_safe' => ['html']]),
        ];
    }

    public function markdown(string $text = null, string $parser = null): string
    {
        if (null === $text) {
            return '';
        }

        return $this->parserManager->transform($text, $parser);
    }

    public function getName()
    {
        return 'markdown';
    }
}
