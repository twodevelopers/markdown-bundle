<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 15:58
 */

namespace TwoDevs\Bundle\MarkdownBundle\Parser\Exception;


use Throwable;

class ParserNotFoundException extends \RuntimeException
{
    public function __construct(string $parserName, array $availableParser = [], $code = 0, Throwable $previous = null)
    {
        $message = sprintf(
            'Unknown parser selected ("%s"), available are: %s',
            $parserName,
            implode(', ', array_keys($availableParser))
        );

        parent::__construct($message, $code, $previous);
    }
}
