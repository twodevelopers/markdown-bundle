<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 15:52
 */

namespace TwoDevs\Bundle\MarkdownBundle\Parser;

use TwoDevs\Bundle\MarkdownBundle\Parser\Exception\ParseException;
use TwoDevs\Bundle\MarkdownBundle\Parser\Exception\ParserNotFoundException;

class ParserManager
{
    /** @var ParserInterface[] */
    private $parsers = [];

    /**
     * @param ParserInterface $parser
     * @param string|null $alias
     */
    public function addParser(ParserInterface $parser, string $alias = null)
    {
        if (null === $alias) {
            $alias = get_class($parser);
        } else {
            $this->parsers[get_class($parser)] = $parser;
        }

        $this->parsers[$alias] = $parser;
    }

    /**
     * @param string $text
     * @param string|null $parserName
     * @return string
     *
     * @throws ParseException
     */
    public function transform(string $text, string $parserName = null): string
    {
        if (null === $parserName) {
            $parserName = 'default';
        }

        if (!isset($this->parsers[$parserName])) {
            throw new ParserNotFoundException($parserName, $this->parsers);
        }

        try {
            return $this->parsers[$parserName]->transformMarkdown($text);
        } catch (\Exception $exp) {
            throw new ParseException('Error while parsing markdown', 0, $exp);
        }
    }
}
