<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 15:41
 */

namespace TwoDevs\Bundle\MarkdownBundle\Parser;

/**
 * Interface ParserInterface
 * @package TwoDevs\Bundle\MarkdownBundle\Parser
 */
interface ParserInterface
{
    /**
     * Transform a Markdown string
     *
     * @param string $text
     * @return string
     */
    public function transformMarkdown(string $text): string;
}
