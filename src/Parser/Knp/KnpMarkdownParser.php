<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 15:45
 */

namespace TwoDevs\Bundle\MarkdownBundle\Parser\Knp;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use TwoDevs\Bundle\MarkdownBundle\Parser\ParserInterface;

class KnpMarkdownParser implements ParserInterface
{
    /** @var MarkdownParserInterface */
    private $parser;

    /**
     * KnpMarkdownParser constructor.
     * @param MarkdownParserInterface $parser
     */
    public function __construct(MarkdownParserInterface $parser)
    {
        $this->parser = $parser;
    }

    /**
     * {@inheritdoc}
     */
    public function transformMarkdown(string $text): string
    {
        return (string) $this->parser->transformMarkdown($text);
    }
}
