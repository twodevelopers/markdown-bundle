<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 15:42
 */

namespace TwoDevs\Bundle\MarkdownBundle\Parser\Knp;

use Knp\Bundle\MarkdownBundle\Parser\ParserManager;
use TwoDevs\Bundle\MarkdownBundle\Parser\ParserInterface;

class KnpMarkdownManagerParser implements ParserInterface
{
    /** @var ParserManager */
    private $markdownManager;

    /**
     * KnpMarkdownParser constructor.
     * @param ParserManager $markdownManager
     */
    public function __construct(ParserManager $markdownManager)
    {
        $this->markdownManager = $markdownManager;
    }

    /**
     * {@inheritdoc}
     */
    public function transformMarkdown(string $text): string
    {
        return (string) $this->markdownManager->transform($text);
    }
}
