<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 14:04
 */

namespace TwoDevs\Bundle\MarkdownBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TwoDevs\Bundle\MarkdownBundle\Editor\EditorManager;

class MarkdownEditorType extends AbstractType
{
    /**
     * @var EditorManager
     */
    private $editorManager;

    /**
     * MarkdownEditorType constructor.
     * @param EditorManager $editorManager
     */
    public function __construct(EditorManager $editorManager)
    {
        $this->editorManager = $editorManager;
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $this->editorManager->getEditor($options['editor'])->finishView($view, $form, $options);
        parent::finishView($view, $form, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->editorManager->getEditor($options['editor'])->buildForm($builder, $options);
        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $this->editorManager->getEditor($options['editor'])->buildView($view, $form, $options);
        $view->vars = array_replace($view->vars, [
            'show_help' => $options['show_help']
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'editor' => 'bootstrap-markdown',
            'show_help' => false,
            'attr' => [
                'class' => 'markdown-editor',
                'rows'  => 15,
            ]
        ]);

        $this->editorManager->configureFormOptions($resolver);

        $resolver->addAllowedValues('editor', $this->editorManager->getAvailableEditors());
    }

    public function getBlockPrefix()
    {
        return 'markdown_editor';
    }

    public function getParent()
    {
        return TextareaType::class;
    }
}
