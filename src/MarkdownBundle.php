<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 14:01
 */

namespace TwoDevs\Bundle\MarkdownBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TwoDevs\Bundle\MarkdownBundle\DependencyInjection\Compiler\EditorCompilerPass;
use TwoDevs\Bundle\MarkdownBundle\DependencyInjection\Compiler\ParsersCompilerPass;
use TwoDevs\Bundle\MarkdownBundle\Editor\EditorInterface;
use TwoDevs\Bundle\MarkdownBundle\Parser\ParserInterface;

class MarkdownBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ParsersCompilerPass());
        $container->addCompilerPass(new EditorCompilerPass());
    }
}
