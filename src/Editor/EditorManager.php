<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 22.11.2017
 * Time: 12:57
 */

namespace TwoDevs\Bundle\MarkdownBundle\Editor;


use Symfony\Component\OptionsResolver\OptionsResolver;

class EditorManager
{
    /** @var EditorInterface[]|array */
    private $editors = [];

    /**
     * @param EditorInterface $editor
     */
    public function addEditor(EditorInterface $editor)
    {
        $this->editors[$editor->getName()] = $editor;
    }

    /**
     * @return EditorInterface[]
     */
    public function getEditors(): array
    {
        return $this->editors;
    }

    /**
     * @return array
     */
    public function getAvailableEditors(): array
    {
        return array_keys($this->editors);
    }

    /**
     * @param string $name
     * @return EditorInterface
     */
    public function getEditor(string $name): EditorInterface
    {
        if (!isset($this->editors[$name])) {
            throw new \InvalidArgumentException('Editor was not found!');
        }

        return $this->editors[$name];
    }

    public function configureFormOptions(OptionsResolver $resolver)
    {
        foreach ($this->editors as $editor) {
            $editor->addConfiguration($resolver);
        }
    }
}
