<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 22.11.2017
 * Time: 12:53
 */

namespace TwoDevs\Bundle\MarkdownBundle\Editor;


use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

interface EditorInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options);
    public function buildView(FormView $view, FormInterface $form, array $options);
    public function finishView(FormView $view, FormInterface $form, array $options);

    public function addConfiguration(OptionsResolver $resolver);
    public function getName(): string;
}