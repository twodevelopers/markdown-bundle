<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 22.11.2017
 * Time: 12:55
 */

namespace TwoDevs\Bundle\MarkdownBundle\Editor;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BootstrapMarkdownEditor extends AbstractEditor
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $provider = $options['bootstrap_markdown']['style'] == 'normal' ? 'markdown' : 'markdown-editable';

        $view->vars['attr'] = array_merge($view->vars['attr'], [
            'data-provide' => $provider,
        ]);
    }

    public function addConfiguration(OptionsResolver $resolver)
    {
        $resolver->setDefault('bootstrap_markdown', [
            'style' => 'normal'
        ]);

        $resolver->setAllowedValues('bootstrap_markdown', function(array $value) {
             if (!in_array($value['style'], ['normal', 'inline'])) {
                 return false;
             }

             return true;
        });
    }

    public function getName(): string
    {
        return 'bootstrap-markdown';
    }
}