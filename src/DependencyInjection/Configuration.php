<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 16:09
 */

namespace TwoDevs\Bundle\MarkdownBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder->root('twodevs_markdown', 'array')
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('enable_twig')->defaultTrue()->end()
                ->arrayNode('parser')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('service')->cannotBeEmpty()->defaultValue('TwoDevs\\Bundle\\MarkdownBundle\\Parser\\Michelf\\Preset\\Max')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
