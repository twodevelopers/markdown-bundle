<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 16:14
 */

namespace TwoDevs\Bundle\MarkdownBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use TwoDevs\Bundle\MarkdownBundle\Editor\EditorManager;
use TwoDevs\Bundle\MarkdownBundle\Parser\ParserManager;

class EditorCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(EditorManager::class)) {
            return;
        }

        if (!($definition = $container->findDefinition(EditorManager::class))) {
            return;
        }

        $definition = $container->getDefinition(EditorManager::class);

        foreach ($container->findTaggedServiceIds('md.editor') as $id => $tags) {
            $definition->addMethodCall('addEditor', [new Reference($id)]);
        }
    }
}
