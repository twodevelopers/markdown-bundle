<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 16:14
 */

namespace TwoDevs\Bundle\MarkdownBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use TwoDevs\Bundle\MarkdownBundle\Parser\ParserManager;

class ParsersCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(ParserManager::class)) {
            return;
        }

        if (!($definition = $container->findDefinition(ParserManager::class))) {
            return;
        }

        $defaultAlias = $definition->getTag('md.parser');

        if (!empty($defaultAlias)) {
            $defaultAlias = current($defaultAlias);
            $defaultAlias = isset($defaultAlias['alias']) ? $defaultAlias['alias'] : null;
        }

        $definition = $container->getDefinition(ParserManager::class);

        if (empty($defaultAlias)) {
            $definition->addMethodCall('addParser', [new Reference('md.parser'), 'default']);
        }

        foreach ($container->findTaggedServiceIds('md.parser') as $id => $tags) {
            foreach ($tags as $attributes) {
                $alias = empty($attributes['alias']) ? $id : $attributes['alias'];
                if ($defaultAlias == $alias) {
                    $definition->addMethodCall('addParser', [new Reference($id), 'default']);
                } else {
                    $definition->addMethodCall('addParser', [new Reference($id), $alias]);
                }
            }
        }
    }
}
