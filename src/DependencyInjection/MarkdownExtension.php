<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 21.11.2017
 * Time: 14:03
 */

namespace TwoDevs\Bundle\MarkdownBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class MarkdownExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('parser.yaml');
        $loader->load('editor.yaml');
        $loader->load('form.yaml');

        if ($config['enable_twig']) {
            $loader->load('twig.yaml');
        }

        $container->setAlias('md.parser', $config['parser']['service']);
    }
}
