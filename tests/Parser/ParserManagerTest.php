<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 29.01.2018
 * Time: 19:47
 */

namespace TwoDevs\Bundle\MarkdownBundle\Tests\Parser;


use PHPUnit\Framework\TestCase;
use TwoDevs\Bundle\MarkdownBundle\Parser\ParserManager;

class ParserManagerTest extends TestCase
{
    public function testAddParser()
    {
        $nullParser = new NullParser();

        $refClass = new \ReflectionClass(ParserManager::class);
        $prop = $refClass->getProperty('parsers');
        $prop->setAccessible(true);

        $manager = new ParserManager();
        $manager->addParser($nullParser);

        $this->assertCount(1, $prop->getValue($manager));
        $this->assertEquals($nullParser, $prop->getValue($manager)[NullParser::class]);
    }

    public function testAddParserWithAlias()
    {
        $nullParser = new NullParser();

        $refClass = new \ReflectionClass(ParserManager::class);
        $prop = $refClass->getProperty('parsers');
        $prop->setAccessible(true);

        $manager = new ParserManager();
        $manager->addParser($nullParser, 'default');

        $this->assertCount(2, $prop->getValue($manager));
        $this->assertEquals($nullParser, $prop->getValue($manager)[NullParser::class]);
    }

    public function testTransform()
    {
        $in = "Test Text";
        $nullParser = new NullParser();

        $manager = new ParserManager();
        $manager->addParser($nullParser, 'default');

        $this->assertEquals($in, $manager->transform($in));
    }
}