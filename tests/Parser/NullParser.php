<?php
/**
 * Created by IntelliJ IDEA.
 * User: jensa
 * Date: 29.01.2018
 * Time: 19:46
 */

namespace TwoDevs\Bundle\MarkdownBundle\Tests\Parser;


use TwoDevs\Bundle\MarkdownBundle\Parser\ParserInterface;

class NullParser implements ParserInterface
{
    public function transformMarkdown(string $text): string
    {
        return $text;
    }
}
