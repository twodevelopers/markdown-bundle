Markdown Bundle
===============

Provide a markdown form type, twig extension and parser

You need to inject bootstrap markdown editor style and javasccipts by yourself.

```html
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-markdown/2.10.0/css/bootstrap-markdown.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-markdown/2.10.0/js/bootstrap-markdown.min.js"></script>
```
